import sqlite3, random

# Соединяемся с базой данных, если БД нет, то создается новая с таким именем
con = sqlite3.connect('./database.db')

# Создаем объект курсора
cur = con.cursor()

# Создаем таблицу, если таблица не существует
cur.execute('CREATE TABLE IF NOT EXISTS test(id int, '
'data1 int, '
'data2 int )')

id = int(1)
data1 = int()
data2 = int()

# Заполнение таблицы
for i in range(1000):
    data = [ id, data1, data2]
    id += 1
    data1 = random.randint(0, 1000)
    data2 = random.randint(0, 1000)
    cur.execute('INSERT INTO test VALUES(?, ?, ?)', data)

# Подтверждаем внесение данных в таблицу
con.commit()

# Вывод минимального, среднего, максимального для таблицы data1
with con: 
    cur.execute('SELECT MIN(data1) FROM test')
    _min = cur.fetchall()
    print('Data 1 MIN: ', _min)
with con:  
    cur.execute('SELECT AVG(data1) FROM test')
    _avg = cur.fetchall()
    print('Data 1 AVG: ', _avg)
with con: 
    cur.execute('SELECT MAX(data1) FROM test')
    _max = cur.fetchall()
    print('Data 1 MAX: ', _max)

# и data2
with con: 
    cur.execute('SELECT MIN(data2) FROM test')
    _min = cur.fetchall()
    print('Data 2 MIN: ', _min)
with con:  
    cur.execute('SELECT AVG(data2) FROM test')
    _avg = cur.fetchall()
    print('Data 2 AVG: ', _avg)
with con: 
    cur.execute('SELECT MAX(data2) FROM test')
    _max = cur.fetchall()
    print('Data 2 MAX: ', _max)

cur.close() # Удаляем курсор
con.close() # Разрываем соединение с БД
